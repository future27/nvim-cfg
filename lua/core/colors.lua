vim.opt.termguicolors = true

function SetColor(color)
	color = color or "solarized-osaka"
	vim.cmd.colorscheme(color)

	vim.api.nvim_set_hl(0, "Normal", { bg = "#26282A" })
	vim.api.nvim_set_hl(0, "NormalFloat", { bg = "#353b43" })
	vim.api.nvim_set_hl(0, "ColorColumn", { bg = "none" })
	vim.api.nvim_set_hl(0, "LineNr", { bg = "none" }) -- color of number of line
end

SetColor()
