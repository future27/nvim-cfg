require'nvim-treesitter.configs'.setup {
    ensure_installed = {
        "typescript", "lua", "vim", "tsx", "javascript", "vim", "json", "yaml",
        "html", "css", "dockerfile"
    },

    sync_install = false,
    auto_install = true,
    highight = {enable = true}
}
